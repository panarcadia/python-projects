import random
import os

target_number = random.randrange(1, 11)

os.system("clear")

while True:
    try:
        guess = int(input("Choose a number from 1 to 10: "))
    except ValueError:
        continue

    if guess == target_number:
        print("That's correct!", guess, "is the right number.")
        break
    elif guess < target_number:
        print("That's too low!")
    else:
        print("That's too high!")
