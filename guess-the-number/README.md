# Guess the Number

Console base number guessing game.
The user try's to guess a number between 1 and 10 (inclusive).

```py
import random

os.system("clear")

target_number = random.randrange(1, 10)

while True:
    try:
        guess = int(input("Choose a number from 1 to 10: "))
    except ValueError:
        continue

    if guess == target_number:
        print("That's correct!", guess, "is the right number.")
        break
    elif guess < target_number:
        print("That's too low!")
    else:
        print("That's too high!")
```

Import the random library.

Clear the terminal so that we start a new game at the top of the screen.

Set the target number that the user needs to guess. It's a range of
numbers between 1 an 10 (inclusive).

The program runs in an infinite loop. The loop is broken only when
the user picks the correct number.

The first thing to do in the loop is get user input and try to convert
that input into an integer. 
If that generates an `exception`, `continue` to the top of the loop
to try again until the user inputs a number value.

Now we can use an `if` block to process the user's input.

- If the user guesses the correct number, print a message, and
break out of the loop.
- If the user guesses too low or too high, print a message, and
proceed to the top of the loop.
