def regular_hours(total_hours, max_regular_hours):
    if total_hours < max_regular_hours:
        return total_hours
    return max_regular_hours


def overtime_hours(total_hours, max_regular_hours):
    if total_hours <= max_regular_hours:
        return 0
    return total_hours - max_regular_hours


def regular_pay(hours, rate):
    return hours * rate


def overtime_pay(hours, rate):
    return hours * (rate)


while True:
    try:
        hours = float(input("Enter hours: "))
        rate = float(input("Enter rate: "))
        break
    except ValueError:
        print("Invalid input. Please try again.")

print("*" * 20)

reg_hours = regular_hours(hours, 40)
ot_hours = overtime_hours(hours, 40)
reg_pay = regular_pay(reg_hours, rate)
ot_pay = overtime_pay(ot_hours, rate * 1.5)
total_pay = reg_pay + ot_pay

print("Pay:", total_pay)
