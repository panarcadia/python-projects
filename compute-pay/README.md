# Compute Pay

This program accepts input from the user for *hours* and *rate of pay*,
and then outputs the amount of pay.

We use `try/except` to make sure the input is the correct format. The input is
converted to `float`. If it's not the correct format, we loop until correct.

The **pay** is calculated up to 40 hours. If the **hours** are over 40, the
**rate** of pay is 1.5 times the normal **rate**.

Example Output:

```shell
$ python3 compute-pay.py 
Enter hours: 45
Enter rate: 10
Hours: 40, Rate: 10.0
OT Hours: 5.0, OT Rate 15.0
Overtime Pay: 75.0
Pay: 475.0
```

