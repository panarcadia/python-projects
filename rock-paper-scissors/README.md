# Rock, Paper, Scissors

The Machine chooses rock, paper, or scissors.
The Player does the same.
The result is either a win, loss, or tie.

```
Player Choice: 1 rock, 2 paper, 3 scissors
Player chooses rock, Machine chooses paper
Machine wins!
```
