import random
import os

os.system("clear")

choices = ("Rock", "Paper", "Scissors")
machine_choice = None
player_choice = None
winner = None

while True:
    try:
        input_str = "Player choice: 1 Rock, 2 Paper, 3 Scissors\n"
        player_input = int(input(input_str))
        if player_input < 1 or player_input > 3:
            continue
        player_choice = choices[player_input - 1]
        machine_choice = random.choice(choices)
        break
    except ValueError:
        continue

print("Player Choice:", player_choice)
print("Machine Choice:", machine_choice)

if player_choice == "Rock" and machine_choice == "Scissors":
    winner = "Player Wins!"
elif player_choice == "Paper" and machine_choice == "Rock":
    winner = "Player Wins!"
elif player_choice == "Scissors" and machine_choice == "Paper":
    winner = "Player Wins!"
elif player_choice == machine_choice:
    winner = "It's a tie"
else:
    winner = "Machine Wins!"

print(winner)
