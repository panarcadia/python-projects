def startroom():
    print("You are in the hallway, you can go left or right")
    direction = input()
    if direction == "right":
        secondroom()
    else:
        thirdroom()


def secondroom():
    print("You are in the kitchen, you can go left or right")
    direction = input()
    if direction == "right":
        startroom()
    else:
        print("You meet a huge growling rat... the end...chomp...")
        quit()


def thirdroom():
    print("You are in a hallway, you can go left or right")
    direction = input()
    if direction == "right":
        startroom()
    else:
        secondroom()


startroom()
